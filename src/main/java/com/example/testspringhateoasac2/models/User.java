package com.example.testspringhateoasac2.models;

import org.springframework.hateoas.ResourceSupport;

import java.util.List;

public class User extends ResourceSupport {

    private int id;
    private String name;
    private String gender;

    private List<Comment> comments;

    public User() {
    }

    public User(int id, String name, String gender) {
        this.id = id;
        this.name = name;
        this.gender = gender;
    }

    public User(int id, String name, String gender, List<Comment> comments) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.comments = comments;
    }

    public int getUserId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", comments=" + comments +
                '}';
    }
}
