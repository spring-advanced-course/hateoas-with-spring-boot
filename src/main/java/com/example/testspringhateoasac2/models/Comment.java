package com.example.testspringhateoasac2.models;

import org.springframework.hateoas.ResourceSupport;

public class Comment extends ResourceSupport {

    private int id;
    private String comment;

    private User user;

    public Comment() {
    }

    public Comment(int id, String comment) {
        this.id = id;
        this.comment = comment;
    }

    public Comment(int id, String comment, User user) {
        this.id = id;
        this.comment = comment;
        this.user = user;
    }

    public int getCommentId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", comment='" + comment + '\'' +
                ", user=" + user +
                '}';
    }
}
