package com.example.testspringhateoasac2.controllers;

import com.example.testspringhateoasac2.controllers.restcontrollers.UserRestController;
import com.example.testspringhateoasac2.models.Comment;
import com.example.testspringhateoasac2.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.*;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.naming.ldap.Control;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
//@ExposesResourceFor(User.class)
//@EnableEntityLinks
public class UserController {

    private List<User> userList = new ArrayList<>();
    {
        userList.add(new User(1, "Dara", "male", Arrays.asList(new Comment(1, "Nice Picture"), new Comment(2, "Good News"))));
        userList.add(new User(2, "Sothy", "male", Arrays.asList(new Comment(3, "Beautiful"))));
        userList.add(new User(3, "Lyna", "female", Arrays.asList(new Comment(4, "Clever dog!"))));
    }

    @Autowired
    private EntityLinks entityLinks;


    @GetMapping("/users")
    public ResponseEntity<Resources<User>> findAll() {

        Link link = this.entityLinks.linkToCollectionResource(User.class).withSelfRel();

        Resources<User> resources = new Resources<>(userList, link);

//        resources.add(this.entityLinks.linkFor(User.class).slash("/users").withSelfRel());

        for (User user :
                userList) {
            Integer userId = user.getUserId();

            Link linkToUser = ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(UserRestController.class).getOne(userId)).withRel("self");
//            Link link = this.entityLinks.linkToSingleResource(User.class, userId).withSelfRel();
//            Link link = ControllerLinkBuilder.linkTo(UserController.class).slash(userId).withSelfRel();
            user.removeLinks();
            user.add(linkToUser);



            Link linkToAllComment = ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(UserRestController.class).getAllCommentByUserId(userId)).withRel("allComment");
            user.add(linkToAllComment);
        }

        return new ResponseEntity<>(resources, HttpStatus.OK);
    }




}
