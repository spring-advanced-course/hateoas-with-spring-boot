package com.example.testspringhateoasac2.controllers.restcontrollers;

import com.example.testspringhateoasac2.models.Comment;
import com.example.testspringhateoasac2.models.User;
import org.springframework.hateoas.*;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/api/users")
@ExposesResourceFor(User.class) // if use entityLinks for user
public class UserRestController {

    private EntityLinks entityLinks;

    public UserRestController(EntityLinks entityLinks) {
        this.entityLinks = entityLinks;
    }

    private List<User> userList = new ArrayList<>();

    {
        userList.add(new User(1, "Dara", "male", Arrays.asList(new Comment(1, "Nice Picture"), new Comment(2, "Good News"))));
        userList.add(new User(2, "Sothy", "male", Arrays.asList(new Comment(3, "Beautiful"))));
        userList.add(new User(3, "Lyna", "female", Arrays.asList(new Comment(4, "Clever dog!"))));
    }


    @RequestMapping(value = "", produces = MediaTypes.HAL_JSON_UTF8_VALUE)
    public Map<String, Object> getAll() {

        Map<String, Object> response = new HashMap<>();

        Link link = ControllerLinkBuilder.linkTo(UserRestController.class)
                .withSelfRel();

        for (User user :
                userList) {

            Integer userId = user.getUserId();

            user.removeLinks();

            Link linkToEachUser = ControllerLinkBuilder.linkTo(UserRestController.class)
                    .slash(userId).withRel(Link.REL_SELF);

            user.add(linkToEachUser);

            for (Comment comment :
                    user.getComments()) {
                comment.removeLinks();
                LinkBuilder linkBuilderForComment = entityLinks.linkForSingleResource(Comment.class, comment.getCommentId());
                Link linkForComment = linkBuilderForComment.withSelfRel();
                comment.add(linkForComment);
            }
        }

        response.put("data", userList);
        response.put("link", link);
        return response;
    }

    @GetMapping("/{id}")
    public User getOne(@PathVariable("id") Integer id) {
        for (User user :
                userList) {
            Integer userId = user.getUserId();
            if (userId.equals(id)) {
                user.removeLinks();

                for (Comment comment :
                        user.getComments()) {
                    LinkBuilder linkForComment = entityLinks.linkForSingleResource(Comment.class, comment.getCommentId());
                    comment.add(linkForComment.withSelfRel());
                }
                return user;
            }
        }
        return null;
    }

    @GetMapping("/{id}/comments")
    public ResponseEntity<Resources<Comment>> getAllCommentByUserId(@PathVariable("id") Integer id) {

//        Link linkWithSelf = ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(UserRestController.class).getAllCommentByUserId(id)).withSelfRel();
        Link linkWithSelf = this.entityLinks.linkFor(User.class).slash(id).slash("comments").withSelfRel();

        List<Comment> comments = new ArrayList<>();

        for (User u :
                userList) {
            Integer userId = u.getUserId();
            if (id.equals(userId))
                comments.addAll(u.getComments());
        }

        Resources<Comment> resources = new Resources<>(comments, linkWithSelf);

        return ResponseEntity.ok(resources);
    }
}
