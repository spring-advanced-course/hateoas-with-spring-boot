package com.example.testspringhateoasac2.controllers.restcontrollers;

import com.example.testspringhateoasac2.models.Comment;
import com.example.testspringhateoasac2.models.User;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/api/comments")
@ExposesResourceFor(Comment.class)

public class CommentRestController {

    List<Comment> commentList = new ArrayList<>();

    {
        commentList.add(new Comment(1, "Nice Picture", new User(1, "Dara", "male")));
        commentList.add(new Comment(2, "Good News", new User(1, "Dara", "male")));
        commentList.add(new Comment(3, "Beautiful", new User(2, "Sothy", "male")));
        commentList.add(new Comment(4, "Clever dog!", new User(3, "Lyna", "female")));
    }


    @RequestMapping("")
    public Map<String, Object> getAll() {
        Map<String, Object> response = new HashMap<>();

        Link link = ControllerLinkBuilder.linkTo(CommentRestController.class).withSelfRel();


        for (int i = 0; i < commentList.size(); i++) {
            Link link2 = ControllerLinkBuilder
                    .linkTo(ControllerLinkBuilder.methodOn(CommentRestController.class).getOne(commentList.get(i).getCommentId()))
                    .withSelfRel();

            commentList.get(i).removeLinks();

            commentList.get(i).add(link2);

        }

        response.put("data", commentList);
        response.put("link", link);

        return response;
    }


    @RequestMapping("/{id}")
    public Comment getOne(@PathVariable("id") Integer id) {

        for (int i = 0; i < commentList.size(); i++) {
            if (commentList.get(i).getCommentId() == id)
                return commentList.get(i);
        }
        return null;
    }

}
