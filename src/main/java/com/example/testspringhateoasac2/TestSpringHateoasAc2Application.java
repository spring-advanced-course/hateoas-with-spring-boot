package com.example.testspringhateoasac2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestSpringHateoasAc2Application {

    public static void main(String[] args) {
        SpringApplication.run(TestSpringHateoasAc2Application.class, args);
    }
}
